import React, {useState} from 'react';
import {Link} from 'react-router-dom'
import Logo from '../assets/pizzaLogo.png'
import ReorderIcon from '@material-ui/icons/Reorder';
import './styles/Navbar.css'
import './styles/Home.css'

function Navbar() {
 
  const [openLinks, setOpenLinks] = useState(false)

  const toggleNavbar = () => {
setOpenLinks(!openLinks)
  }

  return (
    
    <div className='navbar'>
        <div className='leftSide' id={openLinks ? 'open' : 'close'}>
         
            <img src={Logo}/>
            <div className='hiddenLinks'>
            <Link to='/'>Home</Link> 
            <Link to='/menu'>Menu</Link>
            <Link to='/about'>About</Link>
            <Link to='/contact'>Contact</Link>
            </div>
            </div>
            {/* <ul className='nav'>
                <li className='nav-link'>
            <Link to='/'>Home</Link>
                </li>
                <li className='nav-link'>
            <Link to='/about'>About</Link>
                </li>
                <li className='nav-link'>
            <Link to='/contact'>Contact</Link>
                </li>
            </ul> */}
            <div className='rightSide'>
              <Link to='/'>Home</Link>
              <Link to='/menu'>Menu</Link>
              <Link to='/about'>About</Link>
              <Link to='/contact'>Contact</Link>
              <button onClick={toggleNavbar}>
              <ReorderIcon/>
              </button>
            </div>
        
        </div>

    
    
  )
}

export default Navbar