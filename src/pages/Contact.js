import React from 'react'
import PizzaLeft from '../assets/pizzaLeft.jpg'
import '../components/styles/Contact.css'

function Contact() {
  return (
    <div className='contact'>
      <div className='leftSide' style={{backgroundImage: `url(${PizzaLeft})`}}></div>
      <div className='rightSide'>
        <h1>CONTACT Us</h1>
        <form id='contact-form' method='POST'>
          <label htmlFor='name'>FULL Name</label>
          <input name='name' placeholder='Enter full name..' type='text'/>
          <label htmlFor='name'>Email</label>
          <input name='name' placeholder='Enter email' type='email'/>
          <label htmlFor='message'>Massage</label>
          <textarea rows='6' placeholder='Enter message...' name='massage' requred></textarea>
          <button type='submit'>Send Message</button>
        </form>
      </div>
    </div>
  )
}

export default Contact